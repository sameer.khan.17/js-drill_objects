const mapObject = require('../mapObject.js');
const testObject = require('../testObject.js');

function addFive(value) {
    return value + 5;
}

const newObject = mapObject(testObject, addFive);
console.log(newObject);