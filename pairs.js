function pairs(obj) {
    let objectPairs = [];
    if (obj) {
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                let pair = [];
                pair.push(key);
                pair.push(obj[key]);
                objectPairs.push(pair);
            }
        }
    }
    return objectPairs;
}
module.exports = pairs;