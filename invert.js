function invert(obj) {
    let invertedObject = {};
    if (obj) {
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                let newKey = obj[key];
                invertedObject[newKey] = key;
            }
        }
    }
    return invertedObject;
}
module.exports = invert;