function mapObject(obj, cb) {
    if (obj == undefined || cb == undefined) {
        return [];
    }
    else {
        let newObject = obj;
        for (const key in obj) {
            if (obj.hasOwnProperty(key) && typeof obj[key] == 'number') {
                newObject[key] = cb(obj[key]);
            }
        }
        return newObject;
    }

}
module.exports = mapObject;