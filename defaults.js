function defaults(obj, defaultProps) {
    if (obj && defaultProps != undefined) {
        for (const key in defaultProps) {
            if (obj.hasOwnProperty(key) == false) {
                obj[key] = defaultProps[key];
            }
        }
    }
    return obj
}
module.exports = defaults; 