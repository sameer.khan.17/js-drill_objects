function keys(obj) {
    let objectKeys = [];
    if (obj) {
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                objectKeys.push(key);
            }
        }
    }
    return objectKeys;
}
module.exports = keys;