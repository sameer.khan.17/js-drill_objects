function values(obj) {
    let objectValues = [];
    if (obj) {
        for (const key in obj) {
            if (obj.hasOwnProperty(key) && typeof obj[key] != 'function') {
                objectValues.push(obj[key]);
            }
        }
    }
    return objectValues;
}
module.exports = values;